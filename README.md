findUserAgentInfo
==================

A plugin for LimeSurvey fo fill some questions with browser information

See the plugin in action on [findUserAgentInfo example survey](http://demonstration.sondages.pro/772229)

## Installation

See [Install and activate a plugin for LimeSurvey](http://extensions.sondages.pro/install-and-activate-a-plugin-for-limesurvey.html)

### Via GIT
- Go to your LimeSurvey Directory (version up to 2.06 only)
- Clone in plugins/findUserAgentInfo directory

### Via ZIP dowload
- Download <http://extensions.sondages.pro/IMG/auto/findUserAgentInfo.zip>
- Extract : `unzip findUserAgentInfo.zip`
- Move the directory to plugins/ directory inside LimeSUrvey

## Home page & Copyright
- HomePage <http://extension.sondages.pro/>
- Copyright © 2014-2016 Denis Chenu <http://sondages.pro>
- Copyright © 2014 Validators <http://validators.nl>
- Browser.php is © Chris Schuld (http://chrisschuld.com/), distributed under [MIT](https://github.com/cbschuld/Browser.php/blob/master/LICENSE.md)

Distributed under [AFFERO GNU GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/agpl.txt) licence.
If you need a more permissive Licence [contact](http://extensions.sondages.pro/about/contact.html).
